﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args) 
        {
            Console.WriteLine(Worker.GetObjectCounts());

            Worker w = new Worker("Gevorg","Zhamharyan",4,19,1,Universities.NUACA,Worker.Title.Bakalavr); 
            Education e = new Education(Universities.NUACA,2021,2025);

            Console.WriteLine(Worker.GetObjectCounts());

            Worker[] W = new Worker[6];

            W[0] = new Worker("Jack","Ma",5,24,5,Universities.AUA,Worker.Title.Aspirant);
            W[1] = new Worker("Nick", "Jonas",7, 65, 14, Universities.YSU,Worker.Title.Bakalavr);
            W[2] = new Worker("Ariana", "Grande",8, 35, 3, Universities.ASUE,Worker.Title.Magistros);
            W[3] = new Worker("Alicia", "Keys",4, 42, 4, Universities.NUACA,Worker.Title.Aspirant);
            W[4] = new Worker("Adam", "Smith",9, 23, 2, Universities.YSU,Worker.Title.Bakalavr);
            W[5] = new Worker("Chris", "Brown",14, 21, 2, Universities.AUA,Worker.Title.Magistros); 

            foreach (var item in W) 
            {
                if (item.years >= 1 && item.years <= 4)
                {
                    Console.WriteLine("Bakalavr");
                }else if (item.years == 5 || item.years == 6)
                {
                    Console.WriteLine("Magistros");
                }else if (item.years == 7 || item.years == 8)
                {
                    Console.WriteLine("Aspirant");
                }
                else
                {
                    Console.WriteLine("Wrong Input Value "); 
                }
            }

            foreach (var item in W) 
            {
                if (item.experience > 5)
                {
                    item.PrintCount(); 
                }
            }

            Console.WriteLine(); 
             
            Worker temp ;

            for(int i = 0; i < W.Length; i++) 
            {
                for(int j = 0; j < W.Length-1; j++)    
                { 
                    if (W[j].age > W[j + 1].age) 
                    {
                        temp = W[j]; 
                        W[j] = W[j + 1];
                        W[j + 1] = temp; 
                    } 
                }
                 
            }

            foreach (var item in W)
            {
                item.PrintSorted(); 
            }
        }
    }
}
