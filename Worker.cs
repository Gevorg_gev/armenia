﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Worker
    {
        public string name;
        public string surname;
        public int age;
        public int experience;
        public Universities universities;
        public Title title;
        public int years;
        private static int count = 0;


        public Worker(string name, string surname,int years, int age, int experience, Universities universities, Title title) 
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.experience = experience;
            this.universities = universities;
            this.title = title;
            this.years = years;
            count++; 
        }

        

        public enum Title
        {
            Bakalavr,
            Magistros,
            Aspirant 
        } 


        public static int GetObjectCounts()
        {
            return count; 
        }

        public void PrintCount()
        {
            Console.WriteLine($"{name} {surname} {age} {experience} {universities} {title} {years}"); 
        } 

        public void PrintSorted()
        {
            Console.WriteLine($"{name} {surname} {age} {experience} {universities} {title} {years}"); 
        }
    }
}
